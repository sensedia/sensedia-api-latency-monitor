var request = require('request');
var fs = require('fs');
var argparse = require('argparse');

var MS_PER_MINUTE = 60000;

main = () => {
    apiLatencyRateCheck()
        .then(content => writeEmail(tsRangeInit, tsRangeEnd, content))
        .catch(error => console.log(error));
}

postRequest = (url, headers, body, acceptedStatus) => {
    let options = { url: url, headers: headers, body: body, method: "POST" };
    return new Promise(
        (resolve, reject) => {
            request.post(options, (error, response, body) => {
                if (error) {
                    let failFact = { error: error, statusCode: null };
                    reject(failFact)
                } else {
                    if (response.statusCode != acceptedStatus) {
                        console.debug("status code1: " + response.statusCode);
                        let failFact = { error: error, statusCode: response.statusCode };
                        reject(failFact);
                    } else {
                        let fact = JSON.parse(body);
                        resolve(fact);
                    }
                }
            });
        }
    );
}

getRequest = (url, headers, acceptedStatus) => {
    let options = { url: url, headers: headers, method: "GET" };
    return new Promise(
        (resolve, reject) => {
            request.get(options, (error, response, body) => {
                if (error) {
                    let failFact = { error: error, statusCode: null };
                    reject(failFact)
                } else {
                    if (response.statusCode != acceptedStatus) {
                        console.debug("status code1: " + response.statusCode);
                        let failFact = { error: error, statusCode: response.statusCode };
                        reject(failFact);
                    } else {
                        let fact = JSON.parse(body);
                        resolve(fact);
                    }
                }
            });
        }
    );
}

rateCheck = (errorCount, totalCount, acceptedThreshold) => {
    if (!totalCount || totalCount == 0 || !errorCount || errorCount == 0) {
        return false;
    }
    if ((errorCount / totalCount) > acceptedThreshold) {
        console.debug("Total erros: " + errorCount + " - Percentual: " + (errorCount / totalCount));
        return true;
    }
    console.debug("Nenhum erro detectado.");
    return false;
}

apiLatencyRateCheck = () => {

    var queryLatency = {
        "size": 0,
        "query": {
            "filtered": {
                "query": {
                    "query_string": {
                        "query": "environmentName: " + config.environment,
                        "analyze_wildcard": true
                    }
                },
                "filter": {
                    "bool": {
                        "must": [
                            {
                                "range": {
                                    "receivedOnDate": {
                                        "gte": tsRangeInit,
                                        "lte": tsRangeEnd,
                                        "format": "epoch_millis"
                                    }
                                }
                            }
                        ],
                        "must_not": []
                    }
                }
            }
        },
        "aggs": {
            "2": {
                "terms": {
                    "field": "apiName",
                    "size": 5,
                    "order": {
                        "_count": "desc"
                    }
                },
                "aggs": {
                    "3": {
                        "range": {
                            "field": "durationMillis",
                            "ranges": [
                                {
                                    "from": null,
                                    "to": config.threshold
                                },
                                {
                                    "from": config.threshold,
                                    "to": null
                                }
                            ],
                            "keyed": true
                        },
                        "aggs": {
                            "4": {
                                "avg": {
                                    "field": "durationMillis"
                                }
                            }
                        }
                    }
                }
            }
        }
    };

    let url = config.url + "/api-metrics/api/v3/trace/search";

    return postRequest(url, headers, JSON.stringify(queryLatency), 200)
        .then((body) => {
            let apis = body["aggregations"]["2"]["buckets"];

            var content = apis.map(api => {
                let lowerKey = "*-" + config.threshold.toFixed(1);
                let higherKey = "" + config.threshold.toFixed(1) + "-*";
                return {
                    name: api["key"],
                    total: api["doc_count"],
                    lower: api["3"]["buckets"][lowerKey]["doc_count"],
                    lowerAvg: api["3"]["buckets"][lowerKey]["4"]["value"],
                    higher: api["3"]["buckets"][higherKey]["doc_count"],
                    higherAvg: api["3"]["buckets"][higherKey]["4"]["value"]
                }
            }).filter(api => {
                return api.total > config.minimum_calls
                    && rateCheck(api.higher, api.total, config.accepted_percentage);
            });

            return new Promise(resolve => {
                if (content && content.length > 0) {
                    resolve(apiErrorTable(content));
                } else {
                    resolve();
                }
            })
        });
}

apiErrorTable = (apis) => {
    var apiTablesHTML =
        "<hr>" +
        "<br>" +
        "<table border='1' cellpadding='0' cellspacing='0'>" +
        "    <tr>" +
        "        <th width='50%'>Nome da API</th>" +
        "        <th width='10%'>Chamadas com Latência Baixa</th>" +
        "        <th width='10%'>Média Latência Baixa</th>" +
        "        <th width='10%'>Chamadas com Latência Alta</th>" +
        "        <th width='10%'>Média Latência Alta</th>" +
        "        <th width='10%'>Total de chamadas</th>" +
        "    </tr>" +
        "{{rows}}" +
        "</table>";

    let row =
        "    <tr>" +
        "        <td>{{apiName}}</td>" +
        "        <td align='center'>{{lower}}</td>" +
        "        <td align='center'>{{lowerAvg}}</td>" +
        "        <td align='center'>{{higher}}</td>" +
        "        <td align='center'>{{higherAvg}}</td>" +
        "        <td align='center'>{{total}}</td>" +
        "    </tr>";
        
    var rows = "";    
    apis.forEach(api => {
        let formatted = row.replace(/{{apiName}}/g, api.name);
        formatted = formatted.replace(/{{lower}}/g, api.lower);
        formatted = formatted.replace(/{{lowerAvg}}/g, api.lowerAvg);
        formatted = formatted.replace(/{{higher}}/g, api.higher);
        formatted = formatted.replace(/{{higherAvg}}/g, api.higherAvg);
        formatted = formatted.replace(/{{total}}/g, api.total);
        rows += formatted;
    });
    apiTablesHTML = apiTablesHTML.replace(/{{rows}}/g, rows);

    return apiTablesHTML;
}

writeEmail = (tsInit, tsEnd, tables) => {
    var emailBody =
        "Resumo de erros no período " +
        "<b>{{dateInit}} a {{dateEnd}} </b>" +
        tables +
        "<br>" +
        "<br>Sensedia - JOB de Monitoramento de Latência";

    emailBody = emailBody.replace(/{{dateInit}}/g, new Date(tsInit).toISOString());
    emailBody = emailBody.replace(/{{dateEnd}}/g, new Date(tsEnd).toISOString());
    emailBody = emailBody.replace(/{{monitorWindowMinutes}}/g, config.monitor_window_minutes);

    let dirname = './output';
    if (tables) {
        if (!fs.existsSync(dirname)) {
            fs.mkdirSync(dirname);
        }
        let filename = dirname + '/arquivo.txt';
        fs.writeFileSync(filename, emailBody);

        console.debug("Arquivo escrito!");
    } else {
        if (fs.existsSync(dirname + '/arquivo.txt')) {
            fs.unlinkSync(dirname + '/arquivo.txt');
        }
    }

    console.debug("Finalizado!");
}

// BEGIN MAIN

var parser = new argparse.ArgumentParser({
    version: '0.0.1',
    addHelp: true,
    description: 'Monitor script of Sensedia APIs'
});

parser.addArgument(['--auth'], { help: 'Chave de autenticação para acesso às APIs de métricas da Sensedia' });
parser.addArgument(['--url'], { help: 'Endereço do Manager da Sensedia' });
parser.addArgument(['--environment'], { help: 'Ambiente onde deve ser executada a operação' });
parser.addArgument(['--window'], { help: 'Janela de tempo para avaliação' });
parser.addArgument(['--maximum_latency'], { help: 'Tempo máximo de latência para chamadas em segundos' });
parser.addArgument(['--accepted_percentage'], { help: 'Percentual aceito de chamadas acima da latência (entre 0 e 1)' });
parser.addArgument(['--minimum_calls'], { help: 'Quantidade mínima de chamadas para avaliação (default 0)' });

var args = parser.parseArgs();

var config = {};
if (args.auth && args.url && args.environment) {
    config = {
        sensedia_auth: args.auth,
        url: args.url,
        environment: args.environment,
        monitor_window_minutes: args.window,
        threshold: (args.maximum_latency * 1000),
        accepted_percentage: args.accepted_percentage,
        minimum_calls: args.minimum_calls ? args.minimum_calls : 0
    };
} else {
    console.log("Command:\n\tnode sensedia-api-latency-monitor.js -h");
    process.exit(-1);
}

var tsRangeEnd = Date.now();
var tsRangeInit = new Date(tsRangeEnd - config.monitor_window_minutes * MS_PER_MINUTE).getTime();

var headers = {
    "Accept": "*/*",
    "Sensedia-Auth": config.sensedia_auth
};

main();