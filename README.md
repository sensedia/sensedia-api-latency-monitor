# sensedia-api-latency-monitor
Jenknis setup for monitoring latency in the API Gateway.

# Requirements
The script is written in NodeJS and it makes use of external libraries `request` and `argparse`.

# Running the script
Run the script with the following command `node sensedia-api-latency-monitor.js -h` to get help and instructions of usage.

The script accepts the following parameters:
- `--auth` is the SensediaAuth token, used for accessing API metrics
- `--url` address of Sensedia's API Manager
- `--environment` filter for environment
- `--window` time window for monitoring high latency
- `--maximum_latency` maximum latency accepted for a call
- `--accepted_percentage` accepted pecentage of high latency calls
- `--minimum_calls` minimum number of calls for latency evaluation (default 0)

When defined, the configuration file will discard the other configuration. Therefore, the use of configuration file and arguments are excluisive.

# Docker container
The repository commit spins a Docker image build and the image can be used as a binary using the following command:
- `docker run anishitani/sensedia-api-latency-monitor -h`

The same arguments from the scripted approach are applied to the containerized version.

Note: The script creates a file with a formatted e-mail body. This file is created as `/ws/output/arquivo.txt`. So the volume should be mounted to recevei this file.

- `docker run -v $PWD/output:/ws/output sensedia/sensedia-api-latency-monitor --auth AUTH --url MANAGER --environment ENVIRONMENT --window WINDOW --maximum_latency MAXIMUM_LATENCY --accepted_percentage MAXIMUM_LATENCY --minimum_calls MINIMUM_CALLS`